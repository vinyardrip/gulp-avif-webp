"use strict";

const util = require("gulp-util");
const PluginError = util.PluginError;
const through = require("through2");
const pluginName = "gulp-avif-webp";

module.exports = () => {
  // support extensions in lower/upper case
  const extensions = [
    ".jpg",
    ".png",
    ".jpeg",
    ".gif",
    ".JPG",
    ".PNG",
    ".JPEG",
    ".GIF",
  ];
  return through.obj(function (file, enc, cb) {
    if (file.isNull()) {
      cb(null, file);
      return;
    }
    if (file.isStream()) {
      cb(new PluginError(pluginName, "Streaming not supported"));
      return;
    }
    try {
      let inPicture = false;
      const data = file.contents
        .toString()
        .split("\n")
        .map(function (line) {
          if (line.indexOf("<picture") + 1) inPicture = true;
          if (line.indexOf("</picture") + 1) inPicture = false;
          if (line.indexOf("<img") + 1 && !inPicture) {
            const Re = /<img([^>]+)src=[\"\'](\S+)[\"\']([^>\/]+)\/?>/gi;
            const regexpArray = Re.exec(line);
            const imgTag = regexpArray[0]; // orig image tag
            const srcImage = regexpArray[2]; // src URL
            let newAvifUrl = srcImage; // for new URL avif
            let newWebpUrl = srcImage; // for new URL webp
            if (srcImage.indexOf(".webp") + 1) return line;
            extensions.forEach((ext) => {
              if (srcImage.indexOf(ext) === -1) {
                return line;
              } else {
                newAvifUrl = newAvifUrl.replace(ext, ".avif");
                newWebpUrl = newWebpUrl.replace(ext, ".webp");
                switch (ext) {
                  case ".jpg":
                    line = `<picture>\n  <source srcset="${newAvifUrl}" type="image/avif">\n  <source srcset="${newWebpUrl}" type="image/webp">\n  ${imgTag}\n</picture>`;
                    break;
                  case ".jpeg":
                    line = `<picture>\n  <source srcset="${newAvifUrl}" type="image/avif">\n  <source srcset="${newWebpUrl}" type="image/webp">\n  ${imgTag}\n</picture>`;
                    break;
                  case ".png":
                    line = `<picture>\n  <source srcset="${newAvifUrl}" type="image/avif">\n  <source srcset="${newWebpUrl}" type="image/webp">\n  ${imgTag}\n</picture>`;
                    break;
                  default:
                    line = imgTag;
                }
              }
            });
            return line;
          }
          return line;
        })
        .join("\n");
      file.contents = new Buffer.from(data);
      this.push(file);
    } catch (err) {
      this.emit("error", new PluginError(pluginName, err));
    }
    cb();
  });
};
